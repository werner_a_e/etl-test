Rails.application.routes.draw do
  resources :importing_units, only: [:index, :new, :create]
end
