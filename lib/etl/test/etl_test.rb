require "test_helper"

class CreateProductTable < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |table|
      table.string :name
      table.integer :cost
      table.timestamps
    end
  end
end

class Product < ActiveRecord::Base
  validates :name, uniqueness: true

  def self.csv?
    true
  end
end

class EtlTest < Minitest::Test
  @@times_run = 0

  def setup
    return unless @@times_run == 0

    ActiveRecord::Base.establish_connection(
      adapter: 'sqlite3',
      database: 'test/test.db'
    )

    CreateProductTable.migrate(:up)
  end

  def test_invalid_record_csv
    @@times_run += 1

    Product.define_singleton_method(:csv_data) do
      "id,name,price\n1,shoes,10.2\n2,hat,5.4\n3,shirt,9.8\n4,pants,12.3\n5,shoes,8"
    end
    Product.define_singleton_method(:mapped_columns) do
      {'id' => 'id', 'name' => 'name', 'price' => 'cost'}
    end
    result_context = Etl::Parse.call(Product)

    assert_equal('Validation failed: Name has already been taken', result_context.message.to_s)
  end

  def test_valid_record_csv
    @@times_run += 1
    Product.delete_all

    Product.define_singleton_method(:csv_data) do
      "id,name,price\n1,shoes,10.2\n2,hat,5.4\n3,shirt,9.8\n4,pants,12.3"
    end
    Product.define_singleton_method(:mapped_columns) do
      {'id' => 'id', 'name' => 'name', 'price' => 'cost'}
    end
    result_context = Etl::Parse.call(Product)

    assert_equal(true, result_context.success?)
  end

  def test_valid_record_api
    @@times_run += 1
    Product.delete_all

    stub_request(:get, 'https://the_erp.com/products')
      .to_return(body: '[{"id":"1","name":"shoes","price":10.9},{"id":"2","name":"hat","price":19}]')

    Product.define_singleton_method(:api?) do
      true
    end
    Product.define_singleton_method(:csv?) do
      false
    end
    Product.define_singleton_method(:api_url) do
      'https://the_erp.com/products'
    end
    Product.define_singleton_method(:mapped_columns) do
      {'id' => 'id', 'name' => 'name', 'price' => 'cost'}
    end
    result_context = Etl::Parse.call(Product)

    assert_equal(true, result_context.success?)
  end

  def test_invalid_columns
    @@times_run += 1

    Product.define_singleton_method(:csv_data) do
      "id,name,price\n1,shoes,10.2\n2,hat,5.4\n3,shirt,9.8\n4,pants,12.3"
    end
    result_context = Etl::Parse.call(Product)

    assert_equal("unknown attribute 'price' for Product.", result_context.message.to_s)
  end

  def teardown
    return unless @@times_run == 4

    CreateProductTable.migrate(:down)
    File.delete('test/test.db')
  end
end