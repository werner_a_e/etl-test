require "etl/version"
require 'active_record'
require 'light-service'
require 'net/http'
require 'csv'
require 'json'

module Etl
  class Parse
    extend LightService::Organizer

    def self.call(model)
      with(model: model).reduce(
          ParseCSVDataAction,
          ConnectApiAction,
          ParseJSONDataAction,
          TransformDataAction,
          SaveToDataBaseAction
        )
    end
  end

  class ParseCSVDataAction
    extend LightService::Action
    expects  :model
    promises :data

    executed do |context|
      unless context.model.respond_to?(:csv?) && context.model.csv?
        context.data = []
        next context
      end

      context.model.csv_data.force_encoding('utf-8')
      csv = CSV.parse(context.model.csv_data, headers: true)

      if csv.empty?
        context.fail!('CSV not present')
        next context
      end

      context.data = csv.map(&:to_h)
    end
  end

  class ConnectApiAction
    extend LightService::Action
    expects  :model
    promises :response_data

    executed do |context|
      unless context.model.respond_to?(:api?) && context.model.api?
        context.response_data = '[]'
        next context 
      end

      uri = URI(context.model.api_url)
      if context.model.respond_to? :api_params
        uri.query = URI.encode_www_form(context.model.api_params)
      end

      response = Net::HTTP.get_response(uri)
      if response.is_a?(Net::HTTPSuccess)
        context.response_data = response.body
      else
        context.fail!('Error getting data from API')
        next context
      end
    end
  end

  class ParseJSONDataAction
    extend LightService::Action
    expects  :model
    promises :data

    executed do |context|
      begin
        unless (context.model.respond_to?(:api?) && context.model.api?) || 
          (context.model.respond_to?(:json?) && context.model.json?) 
          next context 
        end

        context.data = JSON.parse(context.response_data)

        if context.data.empty?
          context.fail!('JSON not present')
          next context
        end
      rescue JSON::ParserError => error
        context.fail!(error)
        next context
      end
    end
  end

  class TransformDataAction
    extend LightService::Action
    expects  :model
    promises :transformed_data

    executed do |context|
      context.transformed_data = []
      unless context.model.respond_to?(:mapped_columns)
        context.transformed_data =
         context.data.first.keys.map { |key| { key => key } }
        next context 
      end

      context.data.each do |raw_item|
        item = {}
        context.model.mapped_columns.each do |origin, destination|
          item[destination] = raw_item[origin]
        end
        context.transformed_data.push item
      end
    end
  end

  class SaveToDataBaseAction
    extend LightService::Action
    expects :model

    executed do |context|
      begin
        context.model.transaction do
          context.transformed_data.each do |item|
            context.model.create!(item)
          end
        end
      rescue ActiveRecord::RecordInvalid, ActiveModel::UnknownAttributeError => error
        context.fail!(error)
        next context
      end
    end
  end
end
