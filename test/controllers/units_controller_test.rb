require 'test_helper'

class UnitsControllerTest < ActionDispatch::IntegrationTest
  test 'shows index page' do
    get importing_units_url
    assert_response :success
  end

  test 'shows new page' do
    get new_importing_unit_url
    assert_response :success
  end

  test 'importing units' do
    unit_file = fixture_file_upload('data/csv/units.csv','text/csv')
    assert_difference('Unit.count', 20) do
      post importing_units_url, params: { file: unit_file }
    end

    assert_redirected_to importing_units_url
  end
end
