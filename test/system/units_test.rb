require "application_system_test_case"

class UnitsTest < ApplicationSystemTestCase
  test 'visiting the index' do
    visit importing_units_url
    assert_selector 'h1', text: 'Units'
  end

  test 'importing units' do
    visit importing_units_url
    click_on 'Import Units'

    attach_file('file', [Rails.root, '/data/csv/units.csv'].join, make_visible: true)
    click_on 'Save'

    assert_text 'Units were successfully imported.'
  end
end
