class Unit < ApplicationRecord
  @@csv_data
  def self.csv?
    true
  end

  def self.csv_data=(value)
    @@csv_data = value
  end

  def self.csv_data
    @@csv_data
  end

  def self.mapped_columns
    {
      'name' => 'name',
      'description' => 'description',
      'area' => 'area',
      'price' => 'price',
      'unit_type' => 'uom'
    }
  end
end
