class ImportingUnitsController < ApplicationController
  def index
    @units = Unit.all
  end

  def new
  end

  def create
    Unit.csv_data = params[:file].read
    result_context = Etl::Parse.call(Unit)

    respond_to do |format|
      if result_context.success?
        format.html { redirect_to importing_units_path, notice: 'Units were successfully imported.' }
      else
        format.html do
          flash[:error] = result_context.message
          render :new
        end
      end
    end
  end
end
